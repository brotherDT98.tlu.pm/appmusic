package com.example.appmusic.utils

class AppConstances {
    companion object {
        const val  MUSIC_FILE:String = "STORED_MUSIC"
        var SHOW_MINI_PLAYER : Boolean = false
        var PATH_URL_ART : String = "PATH_URL_ART"
        var ARTIST_NAME : String = "ARTIST_NAME"
        var SONG_NAME : String = "SONG_NAME"
    }
}