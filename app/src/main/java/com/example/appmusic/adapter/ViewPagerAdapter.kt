package com.example.appmusic.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.appmusic.fragment.DiscoverFragment
import com.example.appmusic.fragment.MusicChartFragment
import com.example.appmusic.fragment.MyMusicFragment
import com.example.appmusic.fragment.RadioFragment

class ViewPagerAdapter(private var fragmentManager: FragmentManager, behavior: Int) :
    FragmentStatePagerAdapter(fragmentManager, behavior) {
    override fun getCount(): Int {
        return 4
    }

    override fun getItem(position: Int): Fragment {
        var fragment:Fragment? = null
        when(position) {
            0 -> fragment = MyMusicFragment.newInstance()
            1 -> fragment =  DiscoverFragment.newInstance()
            2 -> fragment =  MusicChartFragment.newInstance()
            3 -> fragment =  RadioFragment.newInstance()
        }
        return fragment!!
    }
}