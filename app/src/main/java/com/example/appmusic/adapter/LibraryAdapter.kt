package com.example.appmusic.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appmusic.R
import com.example.appmusic.models.Library
import kotlinx.android.synthetic.main.item_library.view.*

class LibraryAdapter(private val listener:OnItemClickListener) : RecyclerView.Adapter<LibraryAdapter.LibraryViewHolder>() {

    var listItem = ArrayList<Library>()

    fun setData(listItem:ArrayList<Library>) {
        this.listItem = listItem
    }

    class LibraryViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val imvLibrary:ImageView = view.imvLibrary
        private val tvLibrary:TextView = view.tvLibrary
        val lnWrapper:LinearLayout = view.lnWrapper

        fun bind(library: Library) {
            imvLibrary.setImageResource(library.image)
            tvLibrary.text = library.title
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LibraryViewHolder {
        return LibraryViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_library,parent,false))
    }

    override fun onBindViewHolder(holder: LibraryViewHolder, position: Int) {
        holder.bind(listItem[position])
        holder.lnWrapper.setOnClickListener {
            listener.onItemCLicked(position)
        }
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    interface OnItemClickListener {
        fun onItemCLicked(position: Int)
    }
}