package com.example.appmusic.fragment

import android.Manifest
import android.content.ContentUris
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.example.appmusic.R
import com.example.appmusic.activity.SongListActivity
import com.example.appmusic.adapter.LibraryAdapter
import com.example.appmusic.models.Audio
import com.example.appmusic.models.Library
import com.example.appmusic.viewmodel.AudioViewModel
import kotlinx.android.synthetic.main.fragment_my_music.*
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions


@Suppress("DEPRECATION")
class MyMusicFragment : Fragment(), LibraryAdapter.OnItemClickListener,
    EasyPermissions.PermissionCallbacks,
    EasyPermissions.RationaleCallbacks {

    private var READ_STORAGE_PERM = 123
    private lateinit var audioViewModel: AudioViewModel

    private var listItem: ArrayList<Library> =
        arrayListOf(
            Library(R.drawable.ic_music, "Bài hát"),
            Library(R.drawable.ic_upload, "Upload"),
            Library(R.drawable.ic_dowload, "Trên thiết bị"),
            Library(R.drawable.ic_mv, "MV"),
            Library(R.drawable.ic_singer, "Nghệ sĩ"),
            Library(R.drawable.ic_music_album, "Album")
        )
    private var libraryAdapter: LibraryAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_my_music, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        libraryAdapter = LibraryAdapter(this)
        rvLibrary.layoutManager =
            GridLayoutManager(activity, 2, GridLayoutManager.HORIZONTAL, false)
        libraryAdapter?.setData(listItem)
        rvLibrary.adapter = libraryAdapter
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            MyMusicFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }

    private fun hasReadStoragePerms(): Boolean {
        return EasyPermissions.hasPermissions(
            requireContext(),
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO
        )
    }


    @RequiresApi(Build.VERSION_CODES.R)
    private fun getMusicFromStorage() {
        val uri: Uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        audioViewModel = ViewModelProvider(activity!!).get(AudioViewModel::class.java)
        println(uri.toString())
        val projection = arrayOf(
            MediaStore.Audio.Media.TITLE,
            MediaStore.Audio.Media.DURATION,
            MediaStore.Audio.Media.DATA,
            MediaStore.Audio.Media.ARTIST,
            MediaStore.Audio.Media.ALBUM_ID
        )
        val cursor = context!!.contentResolver.query(
            uri,
            projection,
            MediaStore.Audio.Media.DATA + " like ? ",
            arrayOf("%Download%"),
            null
        )

        if (cursor != null && cursor.moveToFirst()) {
            do {
                val title: String =
                    cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE))
                val artist: String =
                    cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST))
                val duration: String =
                    cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION))
                val urlMp3: String =
                    cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA))
                val albumId = cursor.getLong(
                    cursor
                        .getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM_ID)
                )

                val sArtworkUri = Uri
                    .parse("content://media/external/audio/albumart")
                val albumArtUri = ContentUris.withAppendedId(sArtworkUri, albumId)

                val audio = Audio(title, duration, artist, urlMp3, albumArtUri.toString())
                audioViewModel.addAudio(audio)
                println(audio.toString())
            } while (cursor.moveToNext())
        } else {
            println("==========================> No data")
        }
        cursor?.close()
    }


    @RequiresApi(Build.VERSION_CODES.R)
    private fun readStorageTask() {
        if (hasReadStoragePerms()) {
            getMusicFromStorage()
            val intent = Intent(activity!!.baseContext, SongListActivity::class.java)
            audioViewModel.getListAudioLiveData().observe(this, {
                intent.putParcelableArrayListExtra("songs", it)
            })
            activity!!.startActivity(intent)
            audioViewModel.clearList()
        } else {
            EasyPermissions.requestPermissions(
                requireActivity(),
                "This app need access to your storage",
                READ_STORAGE_PERM,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO
            )
        }
    }


    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {

    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(requireActivity(), perms)) {
            AppSettingsDialog.Builder(requireActivity()).build().show()
        }
    }

    override fun onRationaleAccepted(requestCode: Int) {

    }

    override fun onRationaleDenied(requestCode: Int) {

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(
            requestCode,
            permissions,
            grantResults,
            requireActivity()
        )
    }

    @RequiresApi(Build.VERSION_CODES.R)
    override fun onItemCLicked(position: Int) {
        Toast.makeText(activity, "Item $position", Toast.LENGTH_SHORT).show()
        when (position) {
            0 -> return
            1 -> return
            2 -> {
                readStorageTask()
            }
            3 -> return
            4 -> return
            5 -> return
        }
    }
}