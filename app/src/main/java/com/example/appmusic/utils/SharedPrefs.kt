package com.example.appmusic.utils

import android.content.Context
import android.content.SharedPreferences

class SharedPrefs(context: Context) {
    private val mSharedPreferences: SharedPreferences =
        context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    private val editor: SharedPreferences.Editor = mSharedPreferences.edit()

    init {
        editor.apply()
    }

    companion object {
        private const val PREFS_NAME = "share_prefs"
        private var mInstance: SharedPrefs? = null

        fun getInstance(context: Context): SharedPrefs {
            mInstance?.let {
                return it
            }
            if (mInstance == null) {
                mInstance = SharedPrefs(context)
            }
            return mInstance!!
        }
    }

    fun clear() {
        mSharedPreferences.edit().clear().apply()
    }

//    fun setLogin(login: Boolean) {
//        editor.putBoolean(AppConstants.KEY_LOGIN, login)
//        editor.commit()
//    }
//    fun getLogin(): Boolean {
//        return mSharedPreferences.getBoolean(AppConstants.KEY_LOGIN, false)
//    }

    fun setLastPlayed(song: String) {
        editor.putString(AppConstances.MUSIC_FILE, song)
        editor.commit()
    }
    fun getLastPlayed() : String? {
        return mSharedPreferences.getString(AppConstances.MUSIC_FILE, "")
    }

    fun setUrlImage(url: String) {
        editor.putString(AppConstances.PATH_URL_ART, url)
        editor.commit()
    }
    fun getUrlImage() : String? {
        return mSharedPreferences.getString(AppConstances.PATH_URL_ART, "")
    }

    fun setSongName(name: String) {
        editor.putString(AppConstances.SONG_NAME, name)
        editor.commit()
    }

    fun getSongName() : String? {
        return mSharedPreferences.getString(AppConstances.SONG_NAME, "")
    }

    fun setArtistName(name: String) {
        editor.putString(AppConstances.ARTIST_NAME, name)
        editor.commit()
    }

    fun getArtistName() : String? {
        return mSharedPreferences.getString(AppConstances.ARTIST_NAME, "")
    }
}