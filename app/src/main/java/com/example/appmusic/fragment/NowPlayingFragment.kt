package com.example.appmusic.fragment


import android.app.PendingIntent
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.appmusic.R
import com.example.appmusic.activity.HomeActivity
import com.example.appmusic.activity.SongListActivity
import com.example.appmusic.models.Audio
import com.example.appmusic.utils.SharedPrefs
import kotlinx.android.synthetic.main.fragment_now_playing.*


class NowPlayingFragment : Fragment() {

    private var sharedPrefs:SharedPrefs? = null
    var songList = ArrayList<Audio>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_now_playing, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedPrefs = SharedPrefs(requireContext())

        lnPlay.setOnClickListener {
            val intent =  Intent(requireContext(), HomeActivity::class.java);
            val pi = PendingIntent.getActivity(requireContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            pi.send()
        }
    }

    override fun onResume() {
        super.onResume()
        tvNameSong.text = sharedPrefs?.getSongName()
        tvArtist.text = sharedPrefs?.getArtistName()
        Glide.with(requireActivity()).asBitmap().load(sharedPrefs?.getUrlImage()).placeholder(R.drawable.ic_mv).into(imvSong1)
    }

}