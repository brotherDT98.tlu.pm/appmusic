package com.example.appmusic.activity

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.appmusic.R
import com.example.appmusic.models.Audio
import com.example.appmusic.service.ActionPlaying
import com.example.appmusic.service.MusicService
import kotlinx.android.synthetic.main.activity_play_music.*


class PlayMusicActivity : AppCompatActivity(), ActionPlaying,ServiceConnection{

    companion object{
        lateinit var songList: ArrayList<Audio>
    }

    private var position = -1
    private lateinit var song: Audio

    private val handler = Handler()
    private lateinit var playThread: Thread
    private lateinit var prevThread: Thread
    private lateinit var nextThread: Thread
    private lateinit var ffThread: Thread
    private lateinit var frThread: Thread

    private  var musicService: MusicService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_play_music)
         getIntentData()
        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                if (musicService != null && p2) {
                    musicService!!.seekTo(p1 * 1000)
                }
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {

            }

            override fun onStopTrackingTouch(p0: SeekBar?) {

            }

        })

        runOnUiThread(object : Runnable {
            override fun run() {
                if (musicService != null) {
                    val currPosition = musicService!!.getCurrentPosition() / 1000
                    seekBar.progress = currPosition
                    tvStart.text = createTime(musicService!!.getCurrentPosition())
                }
                handler.postDelayed(this, 1000)
            }
        })
    }


    private fun startAnim(view: View) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.roate))
    }

    private fun stopAnim(view: View) {
        view.clearAnimation()
    }

    private fun createTime(duration: Int): String {
        var time = ""
        val min = duration / 1000 / 60
        val sec = duration / 1000 % 60

        time += "$min:"
        if (sec < 10) {
            time += "0"
        }
        time += "$sec"

        return time
    }

    private fun setVisualize(audioSessionId: Int) {
        if (audioSessionId != -1) {
            blast.setAudioSessionId(audioSessionId)
        }
    }

    private fun getIntentData() {
        song = intent.getParcelableExtra<Audio>("current_song")!!
        position = intent.getIntExtra("position", -1)
        songList = intent.getParcelableArrayListExtra("songs")!!
        btnPlay.setBackgroundResource(R.drawable.ic_pause)
        intent = Intent(this, MusicService::class.java)
        intent.putExtra("servicePosition", position)
        startService(intent)
    }

    private fun metaData() {
        tvStop.text = createTime(songList[position].duration!!.toInt())
        Glide.with(this).asBitmap().load(songList[position].urlImage).placeholder(R.drawable.ic_mv)
            .into(imvSong1)
        startAnim(imvSong)
    }

    override fun onResume() {

        intent = Intent(this, MusicService::class.java)
        bindService(intent,this, BIND_AUTO_CREATE)

        playThreadBtn()
        prevThreadBtn()
        nextThreadBtn()
        ffThreadBtn()
        frThreadBtn()
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        unbindService(this)
    }

    private fun nextThreadBtn() {
        nextThread = object : Thread() {
            override fun run() {
                btnNext.setOnClickListener {
                    btn_nextClicked()
                }
            }
        }
        nextThread.start()
    }

    private fun prevThreadBtn() {
        prevThread = object : Thread() {
            override fun run() {
                btnPrev.setOnClickListener {
                    btn_prevClicked()
                }
            }
        }
        prevThread.start()
    }

    private fun ffThreadBtn() {
        ffThread = object : Thread() {
            override fun run() {
                btnFF.setOnClickListener {
                    ffBtnClicked()
                }
            }
        }
        ffThread.start()
    }

    private fun frThreadBtn() {
        frThread = object : Thread() {
            override fun run() {
                btnFR.setOnClickListener {
                    frBtnClicked()
                }
            }
        }
        frThread.start()
    }

    private fun playThreadBtn() {
        playThread = object : Thread() {
            override fun run() {
                btnPlay.setOnClickListener {
                    btn_play_pauseClicked()
                }
            }
        }
        playThread.start()
    }

     override fun btn_play_pauseClicked() {
        if (musicService!!.isPlaying()) {
            btnPlay.setBackgroundResource(R.drawable.ic_play)
            musicService!!.showNotification(R.drawable.ic_play)
            musicService!!.pause()
            seekBar.max = musicService!!.getDuration() / 1000
            stopAnim(imvSong)
            runOnUiThread(object : Runnable {
                override fun run() {
                    if (musicService != null) {
                        val currPosition = musicService!!.getCurrentPosition() / 1000
                        seekBar.progress = currPosition
                    }
                    handler.postDelayed(this, 1000)
                }
            })
        } else {
            btnPlay.setBackgroundResource(R.drawable.ic_pause)
            musicService!!.showNotification(R.drawable.ic_pause)
            musicService!!.start()
            seekBar.max = musicService!!.getDuration() / 1000
            startAnim(imvSong)
            setVisualize(musicService!!.getAudioSessionId())
            runOnUiThread(object : Runnable {
                override fun run() {
                    if (musicService != null) {
                        val currPosition = musicService!!.getCurrentPosition() / 1000
                        seekBar.progress = currPosition
                    }
                    handler.postDelayed(this, 1000)
                }
            })
        }
    }

     override fun btn_nextClicked() {
        if (musicService!!.isPlaying()) {
            musicService!!.stop()
            musicService!!.release()
            position = ((position + 1) % songList.size)
            musicService!!.createMediaPlayer(position)
            tvStop.text = createTime(songList[position].duration!!.toInt())
            tvNameSong.text = songList[position].title
            Glide.with(this).asBitmap().load(songList[position].urlImage)
                .placeholder(R.drawable.ic_mv).into(imvSong1)
            startAnim(imvSong)
            setVisualize(musicService!!.getAudioSessionId())
            seekBar.max = musicService!!.getDuration() / 1000
            runOnUiThread(object : Runnable {
                override fun run() {
                    if (musicService != null) {
                        val currPosition = musicService!!.getCurrentPosition() / 1000
                        seekBar.progress = currPosition
                    }
                    handler.postDelayed(this, 1000)
                }
            })
            musicService!!.onCompleted()
            musicService!!.showNotification(R.drawable.ic_pause)
            btnPlay.setBackgroundResource(R.drawable.ic_pause)
            musicService!!.start()
        } else {
            musicService!!.stop()
            musicService!!.release()
            position = ((position + 1) % songList.size)
            musicService!!.createMediaPlayer(position)
            tvStop.text = createTime(songList[position].duration!!.toInt())
            tvNameSong.text = songList[position].title
            Glide.with(this).asBitmap().load(songList[position].urlImage)
                .placeholder(R.drawable.ic_mv).into(imvSong1)
            stopAnim(imvSong)
            seekBar.max = musicService!!.getDuration() / 1000
            runOnUiThread(object : Runnable {
                override fun run() {
                    if (musicService != null) {
                        val currPosition = musicService!!.getCurrentPosition() / 1000
                        seekBar.progress = currPosition
                    }
                    handler.postDelayed(this, 1000)
                }
            })
            musicService!!.onCompleted()
            musicService!!.showNotification(R.drawable.ic_play)
            btnPlay.setBackgroundResource(R.drawable.ic_play)
        }
    }

     override fun btn_prevClicked() {
        if (musicService!!.isPlaying()) {
            musicService!!.stop()
            musicService!!.release()
            position = if ((position - 1) < 0) {
                songList.size - 1
            } else {
                position - 1
            }
            musicService!!.createMediaPlayer(position)
            tvStop.text = createTime(songList[position].duration!!.toInt())
            tvNameSong.text = songList[position].title
            Glide.with(this).asBitmap().load(songList[position].urlImage)
                .placeholder(R.drawable.ic_mv).into(imvSong1)
            startAnim(imvSong)
            setVisualize(musicService!!.getAudioSessionId())
            seekBar.max = musicService!!.getDuration() / 1000
            runOnUiThread(object : Runnable {
                override fun run() {
                    if (musicService != null) {
                        val currPosition = musicService!!.getCurrentPosition() / 1000
                        seekBar.progress = currPosition
                    }
                    handler.postDelayed(this, 1000)
                }
            })
            musicService!!.onCompleted()
            musicService!!.showNotification(R.drawable.ic_pause)
            btnPlay.setBackgroundResource(R.drawable.ic_pause)
            musicService!!.start()
        } else {
            musicService!!.stop()
            musicService!!.release()
            position = if ((position - 1) < 0) {
                songList.size - 1
            } else {
                position - 1
            }
            musicService!!.createMediaPlayer(position)
            tvStop.text = createTime(songList[position].duration!!.toInt())
            tvNameSong.text = songList[position].title
            Glide.with(this).asBitmap().load(songList[position].urlImage)
                .placeholder(R.drawable.ic_mv).into(imvSong1)
            stopAnim(imvSong)
            seekBar.max = musicService!!.getDuration() / 1000
            runOnUiThread(object : Runnable {
                override fun run() {
                    if (musicService != null) {
                        val currPosition = musicService!!.getCurrentPosition() / 1000
                        seekBar.progress = currPosition
                    }
                    handler.postDelayed(this, 1000)
                }
            })
            musicService!!.onCompleted()
            musicService!!.showNotification(R.drawable.ic_play)
            btnPlay.setBackgroundResource(R.drawable.ic_play)
        }
    }

    private fun ffBtnClicked() {
        musicService!!.seekTo(musicService!!.getCurrentPosition() + 10000)
    }

    private fun frBtnClicked() {
        musicService!!.seekTo(musicService!!.getCurrentPosition() - 10000)
    }

    override fun onDestroy() {
        if (blast != null) {
            blast.release()
        }
        super.onDestroy()
    }

    override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
        val mBinder:MusicService.MyBinder = service as MusicService.MyBinder

        musicService = mBinder.getService()
        musicService!!.setCallBack(this)
        Toast.makeText(this, "connected $musicService", Toast.LENGTH_SHORT).show()

        seekBar.max = musicService!!.getDuration() / 1000
        setVisualize(musicService!!.getAudioSessionId())
        //metadata
        metaData()

        tvNameSong.isSelected = true
        tvNameSong.text = songList[position].title
        musicService!!.onCompleted()
        musicService!!.showNotification(R.drawable.ic_pause)
    }

    override fun onServiceDisconnected(p0: ComponentName?) {
        musicService = null
    }

}