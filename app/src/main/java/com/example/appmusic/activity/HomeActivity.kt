package com.example.appmusic.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.appmusic.R
import com.example.appmusic.adapter.ViewPagerAdapter
import com.example.appmusic.utils.SharedPrefs
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {
    private var viewPagerAdapter: ViewPagerAdapter? = null
    private var sharedPrefs:SharedPrefs? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        sharedPrefs = SharedPrefs(this)

        setUpViewPager()

        bottom_nav.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.item1 -> vPWrapper.currentItem = 0
                R.id.item2 -> vPWrapper.currentItem = 1
                R.id.item3 -> vPWrapper.currentItem = 2
                R.id.item4 -> vPWrapper.currentItem = 3
            }
            true
        }
    }

    private fun setUpViewPager() {
        viewPagerAdapter = ViewPagerAdapter(
            supportFragmentManager,
            FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
        )
        vPWrapper.adapter = viewPagerAdapter
        vPWrapper.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> bottom_nav.menu.findItem(R.id.item1).isChecked = true
                    1 -> bottom_nav.menu.findItem(R.id.item2).isChecked = true
                    2 -> bottom_nav.menu.findItem(R.id.item3).isChecked = true
                    3 -> bottom_nav.menu.findItem(R.id.item4).isChecked = true
                }
            }

            override fun onPageScrollStateChanged(state: Int) {

            }

        })
    }

}