package com.example.appmusic.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.appmusic.models.Audio

class AudioViewModel : ViewModel() {

    private var mutableLiveDataAudio: MutableLiveData<ArrayList<Audio>> = MutableLiveData()
    private lateinit var mListAudio: ArrayList<Audio>

    init {
        setData()
    }

    private fun setData() {
        mListAudio = ArrayList()
        mutableLiveDataAudio.value = mListAudio
    }

    fun getListAudioLiveData(): MutableLiveData<ArrayList<Audio>> {
        return mutableLiveDataAudio
    }

    fun addAudio(audio: Audio) {
        mListAudio.add(audio)
        mutableLiveDataAudio.value = mListAudio
    }

    fun clearList() {
        mListAudio.clear()
        mutableLiveDataAudio.value = mListAudio
    }

}