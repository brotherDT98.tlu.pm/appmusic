package com.example.appmusic.models

import android.net.Uri
import android.os.Parcel
import android.os.Parcelable

data class Audio(val title:String? = "", val duration:String? = "", val artist:String? = "", val urlMp3: String? = "", val urlImage:String? = "") :Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(duration)
        parcel.writeString(artist)
        parcel.writeString(urlMp3)
        parcel.writeString(urlImage)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return "Audio(title=$title, duration=$duration, artist=$artist, urlMp3=$urlMp3, urlImage=$urlImage)"
    }

    companion object CREATOR : Parcelable.Creator<Audio> {
        override fun createFromParcel(parcel: Parcel): Audio {
            return Audio(parcel)
        }

        override fun newArray(size: Int): Array<Audio?> {
            return arrayOfNulls(size)
        }
    }

}

