package com.example.appmusic

import android.app.Application
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build

class ApplicationClass : Application() {
    companion object {
        const val ACTION_OPEN = "actionopen"
        const val ACTION_REMOVE = "actionremove"
        const val ACTION_PREV = "actionprev"
        const val ACTION_NEXT = "actionnext"
        const val ACTION_PLAY = "actionplay"
        const val CHANNEL_ID_1 = "chanel1"
        const val CHANNEL_ID_2 = "chanel2"
    }


    override fun onCreate() {
        super.onCreate()
        createNotificationChannel()
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel1 =
                NotificationChannel(CHANNEL_ID_1, "Channel(1)", NotificationManager.IMPORTANCE_HIGH)
            channel1.description = "Channel 1 desc"

            val channel2 =
                NotificationChannel(CHANNEL_ID_2, "Channel(2)", NotificationManager.IMPORTANCE_HIGH)
            channel1.description = "Channel 2 desc"

            val notificationManager = getSystemService(NotificationManager::class.java)

            notificationManager.createNotificationChannel(channel1)
            notificationManager.createNotificationChannel(channel2)
        }
    }
}