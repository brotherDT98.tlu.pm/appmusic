package com.example.appmusic.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.appmusic.R


class MusicChartFragment : Fragment() {



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_music_chart, container, false)
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            MusicChartFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}