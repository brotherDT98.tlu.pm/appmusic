package com.example.appmusic.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.appmusic.R
import com.example.appmusic.models.Audio
import kotlinx.android.synthetic.main.item_song_list.view.*

class SongListAdapter(private val listener: OnItemClickListener, private val mContext: Context) :
    RecyclerView.Adapter<SongListAdapter.SongListViewHolder>() {

    var songList = ArrayList<Audio>()

    fun setData(songList: ArrayList<Audio>) {
        this.songList = songList
        notifyDataSetChanged()
    }

    inner class SongListViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val tvNameSong: TextView = view.tvNameSong
        private val tvArtist: TextView = view.tvArtist
        val lnWrapper: LinearLayout = view.lnWrapper
        private val imvSong: ImageView = view.imvSong
        fun bind(audio: Audio) {
            tvNameSong.isSelected = true
            tvNameSong.text = audio.title
            tvArtist.text = audio.artist
            Glide.with(mContext).asBitmap().placeholder(R.drawable.ic_mv).load(audio.urlImage)
                .into(imvSong)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongListViewHolder {
        return SongListViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_song_list, parent, false)
        )
    }

    override fun onBindViewHolder(holder: SongListViewHolder, position: Int) {
        holder.bind(songList[position])
        holder.lnWrapper.setOnClickListener {
            listener.onItemClicked(position)
        }
    }

    override fun getItemCount(): Int {
        return songList.size
    }

    interface OnItemClickListener {
        fun onItemClicked(position: Int)
    }

}