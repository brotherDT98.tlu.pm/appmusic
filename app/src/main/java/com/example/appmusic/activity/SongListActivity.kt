package com.example.appmusic.activity

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.appmusic.R
import com.example.appmusic.adapter.SongListAdapter
import com.example.appmusic.fragment.NowPlayingFragment
import com.example.appmusic.models.Audio
import com.example.appmusic.utils.SharedPrefs
import kotlinx.android.synthetic.main.activity_song_list.*

class SongListActivity : AppCompatActivity(), SongListAdapter.OnItemClickListener {


    var songList: ArrayList<Audio>? = null


    private var songListAdapter: SongListAdapter? = null
    private val MY_DATA = 111
    private var sharedPrefs:SharedPrefs? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_song_list)
        supportFragmentManager.beginTransaction().add(R.id.fragPlayer, NowPlayingFragment()).commit()
        sharedPrefs = SharedPrefs(this)
        songListAdapter = SongListAdapter(this, this)

        songList = intent.getParcelableArrayListExtra("songs")!!
        if (songList != null) {
            songListAdapter?.setData(songList as java.util.ArrayList<Audio>)
        }
        tv1.text = songList?.size.toString()

        rvSongList.layoutManager = LinearLayoutManager(this)
        rvSongList.setHasFixedSize(true)
        rvSongList.adapter = songListAdapter


    }

    override fun onItemClicked(position: Int) {
        Toast.makeText(this, "Item $position", Toast.LENGTH_SHORT).show()
        intent = Intent(this, PlayMusicActivity::class.java)
        intent.putExtra("current_song", songList?.get(position))
        intent.putParcelableArrayListExtra("songs", songList)
        intent.putExtra("position", position)
        startActivityForResult(intent, MY_DATA)

    }

}