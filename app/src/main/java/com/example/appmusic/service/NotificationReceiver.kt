package com.example.appmusic.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.appmusic.ApplicationClass.Companion.ACTION_NEXT
import com.example.appmusic.ApplicationClass.Companion.ACTION_OPEN
import com.example.appmusic.ApplicationClass.Companion.ACTION_PLAY
import com.example.appmusic.ApplicationClass.Companion.ACTION_PREV

class NotificationReceiver:BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val actionName:String = intent?.action.toString()
        val serviceIntent = Intent(context, MusicService::class.java)
        when(actionName) {
            ACTION_PLAY -> {
                serviceIntent.putExtra("ActionName","playPause")
                context?.startService(serviceIntent)
            }
            ACTION_PREV -> {
                serviceIntent.putExtra("ActionName","prev")
                context?.startService(serviceIntent)
            }
            ACTION_NEXT -> {
                serviceIntent.putExtra("ActionName","next")
                context?.startService(serviceIntent)
            }
            ACTION_OPEN -> {
                serviceIntent.putExtra("ActionName", "open")
            }
        }
    }
}