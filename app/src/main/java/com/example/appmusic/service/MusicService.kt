package com.example.appmusic.service

import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Binder
import android.os.IBinder
import android.support.v4.media.session.MediaSessionCompat
import android.widget.Toast
import androidx.core.app.NotificationCompat
import com.example.appmusic.ApplicationClass
import com.example.appmusic.R
import com.example.appmusic.activity.PlayMusicActivity
import com.example.appmusic.activity.PlayMusicActivity.Companion.songList
import com.example.appmusic.models.Audio
import com.example.appmusic.utils.SharedPrefs
import kotlinx.android.synthetic.main.activity_play_music.*

class MusicService: Service(), MediaPlayer.OnCompletionListener {

    private val mBinder:IBinder = MyBinder()
    var mediaPlayer: MediaPlayer? = null
    var musicList = ArrayList<Audio>()
    var actionPlaying:ActionPlaying? = null
    var sharedPrefs:SharedPrefs? = null

    private var mediaSessionCompat:MediaSessionCompat? = null


    var position:Int = -1

    override fun onCreate() {
        super.onCreate()
        mediaSessionCompat = MediaSessionCompat(baseContext,"My Audio")
        sharedPrefs = SharedPrefs(this)
    }

    override fun onBind(p0: Intent?): IBinder {
        return mBinder
    }

    inner class MyBinder: Binder() {
        fun getService(): MusicService = this@MusicService
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val mPosition = intent?.getIntExtra("servicePosition",-1)
        val actionName = intent?.getStringExtra("ActionName")
        if (mPosition != -1) {
            playMedia(mPosition!!)
        }
        if (actionName != null) {
            when(actionName) {
                "playPause" -> {
                    Toast.makeText(this,"Play pause", Toast.LENGTH_SHORT).show()
                    if (actionPlaying != null) {
                        actionPlaying!!.btn_play_pauseClicked()
                    }
                }
                "next" -> {
                    Toast.makeText(this,"Next", Toast.LENGTH_SHORT).show()
                    if (actionPlaying != null) {
                        actionPlaying!!.btn_nextClicked()
                    }
                }
                "prev" -> {
                    Toast.makeText(this,"Prev", Toast.LENGTH_SHORT).show()
                    if (actionPlaying != null) {
                        actionPlaying!!.btn_prevClicked()
                    }
                }

            }
        }
        return START_STICKY
    }



    private fun playMedia(startPosition: Int) {
        musicList = songList
        position = startPosition
        if (mediaPlayer != null) {
            mediaPlayer!!.stop()
            mediaPlayer!!.release()
            createMediaPlayer(position)
        } else {
            createMediaPlayer(position)
        }
    }

    fun start() {
        mediaPlayer!!.start()
    }

    fun pause() {
        return mediaPlayer!!.pause()
    }

    fun isPlaying():Boolean {
        return mediaPlayer!!.isPlaying
    }

    fun stop() {
        mediaPlayer!!.stop()
    }

    fun release() {
        mediaPlayer!!.release()
    }

    fun getDuration():Int {
        return mediaPlayer!!.duration
    }

    fun seekTo(position:Int) {
        mediaPlayer!!.seekTo(position)
    }

    fun createMediaPlayer(positionInner: Int) {
        position = positionInner

        sharedPrefs?.setLastPlayed(musicList[positionInner].urlMp3.toString())
        sharedPrefs?.setUrlImage(musicList[positionInner].urlImage.toString())
        sharedPrefs?.setSongName(musicList[positionInner].title.toString())
        sharedPrefs?.setArtistName(musicList[positionInner].artist.toString())

        mediaPlayer = MediaPlayer.create(baseContext,Uri.parse(musicList[position].urlMp3))
        mediaPlayer!!.start()
    }

    fun getAudioSessionId():Int {
        return mediaPlayer!!.audioSessionId
    }

    fun getCurrentPosition():Int {
        return mediaPlayer!!.currentPosition
    }

    fun onCompleted() {
        mediaPlayer!!.setOnCompletionListener(this)
    }

    override fun onCompletion(p0: MediaPlayer?) {
        if (actionPlaying != null) {
            actionPlaying!!.btn_nextClicked()
            if (mediaPlayer != null) {
                createMediaPlayer(position)
                mediaPlayer!!.start()
                onCompleted()
            }
        }

    }

    fun setCallBack(actionPlaying: ActionPlaying) {
        this.actionPlaying = actionPlaying
    }

    fun showNotification(playPauseBtn:Int) {
        val intent = Intent(this, PlayMusicActivity::class.java)
        val contentIntent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_UPDATE_CURRENT)

        val prevIntent = Intent(this,NotificationReceiver::class.java).setAction(ApplicationClass.ACTION_PREV)
        val prevPending = PendingIntent.getBroadcast(this,0,prevIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val pauseIntent = Intent(this,NotificationReceiver::class.java).setAction(ApplicationClass.ACTION_PLAY)
        val pausePending = PendingIntent.getBroadcast(this,0,pauseIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val nextIntent = Intent(this,NotificationReceiver::class.java).setAction(ApplicationClass.ACTION_NEXT)
        val nextPending = PendingIntent.getBroadcast(this,0,nextIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val notification = NotificationCompat.Builder(this, ApplicationClass.CHANNEL_ID_2)
            .setSmallIcon(playPauseBtn)
            .setContentIntent(contentIntent)
            .setContentTitle(musicList[position].title)
            .setContentText(musicList[position].artist)
            .addAction(R.drawable.ic_previous,"Prev", prevPending)
            .addAction(playPauseBtn,"Pause", pausePending)
            .addAction(R.drawable.ic_next,"Next", nextPending)
            .setStyle(androidx.media.app.NotificationCompat.MediaStyle().setMediaSession(mediaSessionCompat?.sessionToken))
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setOnlyAlertOnce(true)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .build()

        startForeground(2, notification)
//        val notificationManager: NotificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
//        notificationManager.notify(0, notification)
    }

}