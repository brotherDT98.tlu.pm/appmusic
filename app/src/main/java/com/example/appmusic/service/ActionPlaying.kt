package com.example.appmusic.service

interface ActionPlaying {
    fun btn_play_pauseClicked()
    fun btn_prevClicked()
    fun btn_nextClicked()
}